﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace ViewChanger.Models
{
    public class Orange: INotifyPropertyChanged
    {
        
        private string title;

        public string Title
        {
            get { return title; }
            set { title = value; PropertyChange(nameof(Title)); }
        }

        private string note;

        public string Note
        {
            get { return note; }
            set { note = value; PropertyChange(nameof(Note)); }
        }


        private DateTime pickedDate;

        public DateTime PickedDate
        {
            get { return pickedDate; }
            set { pickedDate = value; PropertyChange(nameof(PickedDate)); }
        }

        
        public Orange(string title, string note, DateTime pickedDate)
        {
            Title = title;
            Note = note;
            PickedDate = pickedDate;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void PropertyChange(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Globalization;

namespace ViewChanger.Models
{
    public class Blue : INotifyPropertyChanged
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; PropertyChange(nameof(Id)); }
        }


        private string genus;

        public string Genus
        {
            get { return genus; }
            set { genus = value; PropertyChange(nameof(Genus)); }
        }

        private string species;

        public string Species
        {
            get { return species; }
            set { species = value; PropertyChange(nameof(Species)); }
        }

        private string common;
        public string Common
        {
            get { return common; }
            set { common = value; PropertyChange(nameof(Common)); }
        }

        public Blue(int id, string genus, string species, string common)
        {
            id = Id;
            genus = Genus;
            species = Species;
            common = Common;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void PropertyChange(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using ViewChanger.Models;
using ViewChanger.ViewModels;
using ViewChanger.Views;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;

namespace ViewChanger.ViewModels
{
    public class OrangeViewModel
    {
        public ObservableCollection<Orange> Notes { get; set; }


        public OrangeViewModel()
        {
            Notes = new ObservableCollection<Orange>();
        }


        public void AddNote(string title, string note, DateTime pickedDate)
        {

            Notes.Add(new Orange(title, note, pickedDate));
        }

        public async Task WriteToFileAsync()
        {

            string jsonTodo = JsonConvert.SerializeObject(Notes);
            using (StreamWriter writer = new StreamWriter("notes.txt"))
            {
                await writer.WriteAsync(jsonTodo);
            }
            Console.WriteLine("File Loaded.");
        }

        public async Task ReadFromFileAsync()
        {
            string jsonReviews;
            try
            {
                using (StreamReader reader = new StreamReader("notes.txt"))
                {

                    jsonReviews = await reader.ReadToEndAsync();
                    List<Orange> items = JsonConvert.DeserializeObject<List<Orange>>(jsonReviews);

                    foreach (var item in items)
                        Notes.Add(new Orange(item.Title, item.Note, item.PickedDate));

                }
            }
            catch (IOException e)
            {
                Console.WriteLine("Can't load file:");
                Console.WriteLine(e.Message);
            }

        }

    }

}


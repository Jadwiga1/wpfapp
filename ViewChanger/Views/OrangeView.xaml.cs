﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using ViewChanger.ViewModels;
using Newtonsoft.Json;

namespace ViewChanger.Views
{
    /// <summary>
    /// Interaction logic for OrangeView.xaml
    /// </summary>
    public partial class OrangeView : UserControl

    {
        
        public OrangeViewModel vm { get { return DataContext as OrangeViewModel; } }
        
        public OrangeView()
        {
            InitializeComponent();
        }

        private void Button_ClickAdd(object sender, RoutedEventArgs e)
        {
            if (Input_Date.Text == null || Input_Note.Text == null || Input_Title.Text == null)
            {
                MessageBox.Show("Please fill all boxes!");
                return;
            }
            
            vm.AddNote(Input_Title.Text,  Input_Note.Text, (DateTime)Input_Date.SelectedDate);
            Input_Title.Text = "";
            Input_Date.SelectedDate = DateTime.Now;
            Input_Note.Text = "";
            
        }
       
        private async void Button_ClickRead(object sender, RoutedEventArgs e)
        {
            await vm.ReadFromFileAsync();
     
            MessageBox.Show("Loaded");
        }
       
        private async void Button_ClickSave(object sender, RoutedEventArgs e)
        {
            await vm.WriteToFileAsync();
            MessageBox.Show("Written");

        }
    }
}

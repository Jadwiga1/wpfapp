﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ViewChanger.Models;
using ViewChanger.ViewModels;

namespace ViewChanger.Views
{
    /// <summary>
    /// Interaction logic for BlueView.xaml
    /// </summary>
    public partial class BlueView : UserControl
    {
        public BlueViewModel fvm { get { return DataContext as BlueViewModel; } }
        public BlueView()
        {
            InitializeComponent();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            await fvm.AddFromWebAsync();
            
        }
        private void PlantsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
  
}


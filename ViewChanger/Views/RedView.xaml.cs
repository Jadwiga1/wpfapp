﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace ViewChanger.Views
{
    /// <summary>
    /// Interaction logic for RedView.xaml
    /// </summary>
    /// 
    /// 
    /// Image Loader
    public partial class RedView : UserControl
    {
        public RedView()
        {
            InitializeComponent();
        }

        private void ButtonSelectImage_Click(object sender, RoutedEventArgs e)
        {
            //Opening Picture + Openfile + filter
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = false;
            openFileDialog.InitialDirectory = @"c:\";
            openFileDialog.Filter = "Image files (*.png;*.jpeg)|*.png;*.jpeg|All files (*.*)|*.*"; 


            //if something has been entered
            Nullable<bool> dialogOK = openFileDialog.ShowDialog();

            if (dialogOK == true)
            {
                // Create the image element
                string filename = openFileDialog.FileName;
                Source_InputFileName.Text = filename;
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.UriSource = new Uri(filename);
                bi.EndInit();
                ImageBox.Source = bi;

            }


        }

        private void Image_Loader(object sender, RoutedEventArgs e)
        {
            // Create source
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.UriSource = new Uri("d:\\picture.jpg");
            bi.EndInit();
            var image = sender as Image;
            // Set the image source
            image.Source = bi;
        }
    }

}

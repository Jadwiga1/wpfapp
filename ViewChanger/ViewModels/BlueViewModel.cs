﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ViewChanger.Models;

namespace ViewChanger.ViewModels
{
    public class BlueViewModel
    {
        public ObservableCollection<Blue> Plants { get; set; }

        public  BlueViewModel()
        {
            Plants = new ObservableCollection<Blue>();
        }

        public void AddPlants(int id, string genus, string species, string common)
        {
            Plants.Add(new Blue(id, genus, species, common));
        }

        public async Task AddFromWebAsync()
        {
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    HttpResponseMessage response = await client.GetAsync("http://www.plantplaces.com/perl/mobile/viewplantsjson.pl?Combined_Name=RedBud");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    foreach (var item in JObject.Parse(responseBody)["results"])
                    {
                        Console.WriteLine(item);
                        Plants.Add(new Blue((int)(item["id"]), (string)(item["genus"]), (string)(item["species"]), (string)(item["common"])));
                    }

                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine("Error downloading from API:");
                    Console.WriteLine(e.Message);
                }
            }
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewChanger.ViewModels
{
    public class RedViewModel : INotifyPropertyChanged
    {
        private string source;   // the name field
        public string Source    // the Name property
        {
            get { return source; }
            set { source = value; PropertyChange(nameof(Source)); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void PropertyChange(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
